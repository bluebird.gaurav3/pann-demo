"use client";
import React from "react";
import CreateAwesomeSolution from "@components/HomePage/CreateAwesomeSolution";
import IdeaAndDeliverance from "@components/HomePage/IdeaAndDeliverance";
import PerfectServiceSuit from "@components/HomePage/PerfectServiceSuit";
import { homeData } from "@utils/ConstantPageData/Home";
import homeBanner from "../public/assets/images/HomePage/LandingPage.webp";
import HomePageTaglineComponent from "@components/HomePage/HomePageTaglineComponent";
import WhyChooseUs from "@components/HomePage/WhyChooseUs";
import TeamCanTrust from "@components/HomePage/TeamCanTrust";
import BottomToTopComponent from "@components/ButtonComponent/BottomToTopComponent";
import { useRouter } from "next/navigation";

const Home = () => {
  const router = useRouter();

  return (
    <React.Fragment>
      <BottomToTopComponent />
      <HomePageTaglineComponent
        title={homeData.SectionOneHead}
        desc={homeData.SectionOneDesc}
        descTwo={homeData.SectionOneDescTwo}
        onClickAction={() => router.push("/contactus")}
        imgSrc={homeBanner}
      />
      <PerfectServiceSuit />
      <IdeaAndDeliverance onClickAction={() => router.push("/contactus")} />
      <WhyChooseUs />
      <TeamCanTrust />
      <CreateAwesomeSolution onClickAction={() => router.push("/contactus")} />
    </React.Fragment>
  );
};

export default Home;
