const footerConst = {
  marqueHeading: "THANK YOU FOR YOUR VISIT",
  organizationName: "Penn Mobile Solutions",
  organizationFacebook: "Penn Mobile Solutions",
  organizationLinkedIn: "https://www.linkedin.com/company/pennmobilesolutions",
  organizationTwitter: "Penn Mobile Solutions",
  organizationInstagram: "Penn Mobile Solutions",
  organizationAddress:
    "104 S 20th Street,Philadelphia,Pennsylvania, USA - 19103",
  organizationNumber: "+1 (215) 310-0302",
  organizationEmail: "info@pennmobilesolutions.com",
  copyRights: "© 2024 Penn Mobile Solutions. All rights reserved.",
};

export { footerConst };
