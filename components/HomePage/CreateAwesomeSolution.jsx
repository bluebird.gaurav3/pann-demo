"use client";
import { Col, Row } from "antd";
import PropTypes from "prop-types";
import { homeData } from "@utils/ConstantPageData/Home";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";

const CreateAwesomeSolution = (props) => {
  const { onClickAction } = props;
  return (
    <Row
      justify={"center"}
      className="imageContainer1 items-center py-16 px-3 md:p-24"
    >
      <Col xs={24} xl={20}>
        <div className="mx-auto text-center w-10/12">
          <span className="typoStyles500_40 text-white">
            {homeData.SectionSixHead}
            <span className="typoStyles500_40" style={{ color: "#B88DFF" }}>
              {homeData.SectionSixHeadTwo}
            </span>
            .
          </span>{" "}
        </div>
        <p
          className="typoStyles400_30_20 text-white text-center"
          style={{ margin: "2rem 0 3rem 0" }}
        >
          {homeData.SectionSizDesc}
        </p>
        <div
          className="flex justify-center items-center"
          style={{ marginTop: "3.75rem" }}
        >
          <ShadowButtonComp
            btnTitle="Contact Us"
            borderColor={"#FFFFFF"}
            backgroundColor={"#FFFFFF"}
            onClickBtn={onClickAction}
          />
        </div>
      </Col>
    </Row>
  );
};

CreateAwesomeSolution.propTypes = {
  onClickAction: PropTypes.func,
};

export default CreateAwesomeSolution;
