"use client";
import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import Image from "next/image";
import vectorTopRight from "../../public/assets/images/HomePage/homeTopRight.svg";
import vectorLeft from "../../public/assets/images/HomePage/lineLeft.svg";
import vectorRight from "../../public/assets/images/HomePage/lineRight.svg";
import vectorBottom from "../../public/assets/icons/bottom-line.svg";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";

const HomePageTaglineComponent = (props) => {
  const { title, desc, descTwo, onClickAction, imgSrc } = props;
  return (
    <Row
      justify={"center"}
      className="bg-primary-violet relative pt-10 pb-32 md:py-32"
    >
      <Image
        src={vectorTopRight}
        className="home-top-right-vector"
        alt={"Apps For all Screens vector"}
        title={"Apps For all Screens vector"}
      />
      {/************* Mobile View Section ***************/}
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className={`flex justify-center relative mobile-display-style`}
      >
        <div className="home-spread-Big-pink"></div>
        <Image
          src={imgSrc}
          className="w-full h-auto px-12"
          style={{ objectFit: "contain", zIndex: 1 }}
          alt={title}
          title={title}
          loading="eager"
        />
      </Col>
      {/*********** LEFT SECTION *********/}
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className="flex items-center px-2 py-16 md:pl-14 md:pr-2"
      >
        <div>
          <h2 className="typoStyles700_84 mb-8 text-white text-center md:text-left">
            {title}
          </h2>
          <p className="typoStyles_poppins_500_20 mb-8 text-light-text text-center md:text-left">
            {desc}
          </p>
          {descTwo?.length > 0 && (
            <p className="typoStyles_poppins_500_20 mb-8 text-light-text text-center md:text-left">
              {descTwo}
            </p>
          )}
          <div className="flex justify-center md:justify-start mt-9">
            <ShadowButtonComp
              extraCss={"z-10"}
              btnTitle="Contact Us"
              borderColor={"#ffffff"}
              backgroundColor={"#ffffff"}
              onClickBtn={onClickAction}
            />
          </div>
        </div>
      </Col>
      {/************* Desktop View Section ***************/}
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className="relative flex justify-end items-center pr-20 desktop-display-style"
      >
        <div className="home-spread-Big-pink"></div>
        <Image
          src={imgSrc}
          className="home-right-main-img"
          alt={"Apps For All Screen Vector"}
          title={"Apps For All Screen Vector"}
          loading="eager"
        />
      </Col>
      <Image
        src={vectorLeft}
        className="home-center-vector-img"
        alt={"Apps For All Screen Vector"}
        title={"Apps For All Screen Vector"}
      />
      <Image
        src={vectorRight}
        className="home-right-bottom-vector-img"
        alt={"Apps For All Screen Vector"}
        title={"Apps For All Screen Vector"}
      />
      <Image
        src={vectorBottom}
        layout="responsive"
        className="home-vector-right-spread"
        alt={"Apps For All Screen Vector"}
        title={"Apps For All Screen Vector"}
      />
    </Row>
  );
};

HomePageTaglineComponent.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  descTwo: PropTypes.string,
  onClickAction: PropTypes.func,
  imgSrc: PropTypes.string || PropTypes.object,
};

export default HomePageTaglineComponent;
