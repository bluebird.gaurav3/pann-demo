"use client";
import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import Image from "next/image";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";
import { usePathname } from "next/navigation";

const TaglineHeaderComponent = (props) => {
  const pathname = usePathname();
  const { title, desc, descTwo, onClickAction, imgSrc } = props;
  return (
    <Row justify={"center"} className="bg-primary-violet relative">
      {/* h-screen */}
      <div className="home-spread-top-pink"></div>
      <Col
        xs={24}
        md={12}
        className={`flex justify-end relative right-image-display-mobile`}
      >
        <div className="home-spread-Big-pink"></div>
        <Image
          src={imgSrc}
          width={"100%"}
          height={"100%"}
          style={{ objectFit: "contain", zIndex: 1 }}
          alt={title}
          title={title}
          layout="responsive"
          loading="eager"
        />
      </Col>
      <Col
        xs={24}
        md={12}
        className="flex items-center text-center md:text-left py-8 md:pt-0 px-5 md:pl-14 md:pr-2"
      >
        <div>
          <h2 className="typoStyles700_84 mb-8 text-white">{title}</h2>
          <p className="typoStyles_poppins_500_20 mb-8 text-light-text">
            {desc}
          </p>
          {descTwo?.length > 0 && (
            <p className="typoStyles_poppins_500_20 mb-8 text-light-text">
              {desc}
            </p>
          )}

          {pathname.toString() !== "/contactus" ? (
            <div className="md:mt-9 mb-10 md:mb-0 flex justify-center md:justify-start">
              <ShadowButtonComp
                btnTitle={
                  pathname.toString() === "/careers"
                    ? "See Openings"
                    : "Contact Us"
                }
                borderColor={"#ffffff"}
                backgroundColor={"#ffffff"}
                onClickBtn={onClickAction}
              />
            </div>
          ) : (
            <></>
          )}
        </div>
      </Col>
      <Col
        xs={24}
        md={12}
        className={`flex justify-end relative right-image-display-desktop`}
      >
        <div className="home-spread-Big-pink"></div>
        <Image
          src={imgSrc}
          width={"100%"}
          height={"100%"}
          style={{ objectFit: "contain", zIndex: 1 }}
          alt={title}
          title={title}
          layout="responsive"
          // loading="lazy"
          loading="eager"
        />
      </Col>
    </Row>
  );
};

TaglineHeaderComponent.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  descTwo: PropTypes.string,
  onClickAction: PropTypes.func,
  imgSrc: PropTypes.string || PropTypes.object,
};

export default TaglineHeaderComponent;
