"use client";
import Image from "next/image";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import projectImg from "../../public/assets/images/ServicesPage/mailbox.webp";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";
import { serviceData } from "@utils/ConstantPageData/Services";

const StartNewProject = (props) => {
  const { onClickAction } = props;
  return (
    <Row className="bg-primary-violet px-7 md:px-12 lg:px-36 service-setup-project md:pt-14">
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className="md:mt-6 text-center md:text-left"
      >
        <h4 className="typoStyles500_40 text-white">
          {serviceData.SectionFiveHead}
        </h4>
        <p
          className="typoStyles400_30_20 text-white"
          style={{ margin: "2rem 0 3rem 0" }}
        >
          {serviceData.SectionFiveDesc}
        </p>
        <div className="flex items-center justify-center md:items-start md:justify-normal mt-8 md:mt-14 mb-12 md:mb-0">
          <ShadowButtonComp
            btnTitle="Contact Us"
            borderColor={"#FFFFFF"}
            backgroundColor={"#FFFFFF"}
            onClickBtn={onClickAction}
          />
        </div>
      </Col>
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className="flex items-end justify-end"
      >
        <Image
          src={projectImg}
          width={500}
          height={428}
          style={{ objectFit: "contain" }}
          alt={serviceData.SectionFiveHead}
          title={serviceData.SectionFiveHead}
        />
      </Col>
    </Row>
  );
};

StartNewProject.propTypes = {
  onClickAction: PropTypes.func,
};

export default StartNewProject;
