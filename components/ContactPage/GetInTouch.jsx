import React from "react";
import { Col, Row } from "antd";
import Image from "next/image";
import semiSphere from "../../public/assets/icons/Semisphere.svg";
import { contactData } from "@utils/ConstantPageData/Contact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons";
import { footerConst } from "@utils/ConstantPageData/FooterConstantData";
import Link from "next/link";
//

const GetInTouch = () => {
  return (
    <div className="flex justify-center px-6 md:px-14 lg:px-44 pt-12 pb-8 md:py-16 relative bg-primary-creme">
      <Image
        src={semiSphere}
        width={110}
        height={110}
        className="absolute right-0 top-0 hidden md:block"
        alt={contactData.SectionTwoHead}
        title={contactData.SectionTwoHead}
      />
      <div className="w-full">
        <h4 className="typoStyles500_60 mb-7 text-center">
          {contactData.SectionTwoHead}
        </h4>
        <div className="w-full md:w-5/6 md:px-10 mx-auto">
          <p className="typoStyles400_20 text-center text-light-gray-pann-two">
            {contactData.SectionTwoSubDesc}
          </p>
        </div>
        <div className="mt-14 justify-center relative">
          <div className="spreadBox-color"></div>
          <Row justify={"center"}>
            <Col xs={24} md={12} lg={12} xl={12} className="p-2">
              <div className="w-full flex items-center justify-center bg-purple-four py-4 md:py-8">
                <div className="text-center contact-mobile-call">
                  <FontAwesomeIcon
                    icon={faPhone}
                    className="fas faPhone text-2xl px-3 md:text-6xl text-white md:text-black"
                  />
                  <div className="md:mt-5">
                    <Link
                      href={`tel:${footerConst.organizationNumber}`}
                      className="typoStyles500_20 text-white"
                    >
                      {footerConst.organizationNumber}
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} md={12} lg={12} xl={12} className="p-2">
              <div className="w-full flex items-center justify-center bg-light-blue-hover-pann py-4 md:py-8">
                <div className="text-center contact-mobile-call">
                  <FontAwesomeIcon
                    icon={faEnvelope}
                    className="fas faEnvelope text-2xl px-3 md:text-6xl"
                  />
                  <div className="md:mt-5">
                    <Link
                      href={`mailto:${footerConst.organizationEmail}`}
                      className="typoStyles500_20 text-black"
                    >
                      {footerConst.organizationEmail}
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default GetInTouch;
